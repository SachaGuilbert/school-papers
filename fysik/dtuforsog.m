U = [2.088 2.109 2.114 2.101 2.052 2.053 1.801 1.783 1.789];
f = [3e8/(567e-9), 3e8/(567e-9), 3e8/(567e-9),3e8/(585e-9), 3e8/(585e-9), 3e8/(585e-9), 3e8/(660e-9), 3e8/(660e-9), 3e8/(660e-9)];
q = 1.6020e-19;
Eel = U*q;
r = polyfit(f,Eel,1);
x = [440:540];
y = x*r(1)*faktoren+r(2);
faktoren = 1e12;
plot(x,y)
hold on
scatter(f/faktoren,Eel)
xlabel("Giga Hz")
ylabel("Joules")
hold off
